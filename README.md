### What is this repository for? ###

* Dataset for breast histopathology classification
* data/0 - non-IDC, data/1 - IDC
* Obtained from https://www.kaggle.com/paultimothymooney/breast-histopathology-images/data
